apiVersion: unfurl/v1alpha1
kind: Ensemble
spec:
  shared:
    compute:
      dockerhost:
        type: unfurl.nodes.HttpsProxyContainerComputeHost
  deployment_blueprints:
    +include:
      file: deployment-blueprints.yaml
      repository:
        name: types
        url: https://unfurl.cloud/onecommons/unfurl-types.git
  service_template:
    description: A version of VS Code that runs a server on a remote machine and allows
      access through a modern web browser.
    metadata:
      template_name: Openvscode Server
      template_version: '0.1'
      image: '# url to image'
      primaryDeploymentBlueprint: gcp
      displayName: OpenVSCode Server
      description: A version of VS Code that runs a server on a remote machine and
        allows access through a modern web browser.
      isOfficial: true
      documentation: See https://github.com/linuxserver/docker-openvscode-server
      instructions:
        start: |-
          OpenVSCode Server provides a version of VS Code that runs a server on a remote machine and allows access through a modern web browser.
          It's based on the very same architecture used by Gitpod or GitHub Codespaces at scale.
        end: >
          OpenVSCode Server has been successfully deployed! It might take few moments
          before it's fully started.
          Please turn on "Websocket Support" in the settings.
          You can access it at http://$$cap_appname.$$cap_root_domain
          If you are using a connection secret or connection token, append ?tkn=$$cap_openvscode_server_connection_token
          to the URL.
    repositories:
      types:
        url: https://unfurl.cloud/onecommons/unfurl-types.git
    imports:
    - file: service-template.yaml
      repository: types
    node_types:
      OpenvscodeServerApp:
        derived_from: unfurl.nodes.WebApp
        properties:
          openvscode_server_version:
            type: string
            title: Version Tag
            description: Check out their documentation for the valid tags https://github.com/linuxserver/docker-openvscode-server/#version-tags
            default: 1.74.3
            constraints:
            - pattern: ^([^\s^\/])+$
            metadata:
              user_settable: true
          openvscode_server_timezone:
            type: string
            title: Timezone
            description: Timezone for the application, find yours at https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
            default: UTC
            constraints:
            - pattern: .{1,}
            metadata:
              user_settable: true
          openvscode_server_user_id:
            type: string
            title: User ID
            description: User ID that the process uses, run (id $user) on your instance
              to see the ID
            default: '1000'
            constraints:
            - pattern: .{1,}
            metadata:
              user_settable: true
          openvscode_server_group_id:
            type: string
            title: Group ID
            description: Group ID that the process uses, run (id $user) on your instance
              to see the ID
            default: '1000'
            constraints:
            - pattern: .{1,}
            metadata:
              user_settable: true
          openvscode_server_connection_token:
            type: string
            title: Connection Token
            description: Security token for accessing the Web UI. Should only contain
              alphanumeric characters or _, -. Leave empty to disable.
            default:
              _generate:
                ranges:
                - - 48
                  - 57
                - - 97
                  - 102
                len: 16
            metadata:
              user_settable: true
              sensitive: true
          openvscode_server_connection_secret:
            type: string
            title: Connection Secret
            description: Optional path to a file inside the container that contains
              the security token for accessing the Web UI (ie. /path/to/file). Overrides
              Connection Token. Leave empty to disable.
            default:
              _generate:
                preset: password
            metadata:
              user_settable: true
              sensitive: true
          openvscode_server_sudo_password:
            type: string
            title: Sudo Password
            description: User will have sudo access in the terminal with the specified
              password. Leave empty to disable.
            default:
              _generate:
                preset: password
            metadata:
              user_settable: true
              sensitive: true
          openvscode_server_sudo_hash:
            type: string
            title: Sudo Password Hash
            description: Sudo password via hash (takes priority over Sudo Password).
              Format is "$type$salt$hashed". Leave empty to disable.
            default:
              _generate:
                preset: password
            metadata:
              user_settable: true
              sensitive: true
          subdomain:
            type: string
            title: Subdomain
            description: Choose a subdomain for your deployment. A subdomain of 'www',
              will be at www.your.domain
            default: openvscode-server
            metadata:
              user_settable: true
        requirements:
        - container:
            relationship: unfurl.relationships.Configures
            node: container
    topology_template:
      substitution_mappings:
        node: the_app
      outputs:
        url:
          value:
            eval: ::the_app::url
      node_templates:
        container:
          type: unfurl.nodes.ContainerService
          properties:
            container:
              image: 
                lscr.io/linuxserver/openvscode-server:{{'.configured_by::openvscode_server_version'
                | eval}}
              ports:
              - 3000:3000
              environment:
                eval:
                  to_env:
                    TZ: "{{'.configured_by::openvscode_server_timezone' | eval}}"
                    PUID: "{{'.configured_by::openvscode_server_user_id' | eval}}"
                    PGID: "{{'.configured_by::openvscode_server_group_id' | eval}}"
                    CONNECTION_TOKEN: "{{'.configured_by::openvscode_server_connection_token'
                      | eval}}"
                    CONNECTION_SECRET: "{{'.configured_by::openvscode_server_connection_secret'
                      | eval}}"
                    SUDO_PASSWORD: "{{'.configured_by::openvscode_server_sudo_password'
                      | eval}}"
                    SUDO_PASSWORD_HASH: "{{'.configured_by::openvscode_server_sudo_hash'
                      | eval}}"
              volumes:
              - openvscodeserver-config:/config
          requirements:
          - host:
              node: dockerhost
        the_app:
          type: OpenvscodeServerApp
