# Application Blueprint for Openvscode Server

## What is Openvscode Server?
Openvscode Server is a version of VS Code that runs a server on a remote machine and allows

**Documentation at:** <https://github.com/linuxserver/docker-openvscode-server>

OpenVSCode Server provides a version of VS Code that runs a server on a remote machine and allows access through a modern web browser.
It's based on the very same architecture used by Gitpod or GitHub Codespaces at scale.

This Application Blueprint is made possible by the [Community Maintained One Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from [https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/openvscode-server.yml](https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/openvscode-server.yml).  Changes can instead be applied to `ensemble-template.yaml` to overwrite the generated values.
